export const toLocaldate = (params) => {
    return new Date(params)?.toLocaleDateString();
}
export const toLocalTime = (params) => {

    return   new Date(params)?.toLocaleTimeString();
}

export const startDate = () => {
    let startDate = new Date();
    
    // Bugungi sanaga 30 kunni qo'shib qo'yamiz
    startDate.setDate(startDate.getDate() - 30);
    
    return startDate.toISOString().slice(0, 10);
}
export const endDate = () => {
    return new Date().toISOString().slice(0, 10);
}