
export const saveShopeToLocalStorage = (params) => {
    localStorage.setItem("selectShop",JSON.stringify(params));
}

export const getShopeToLocalStorage = () => {
   return JSON.parse(localStorage.getItem("selectShop"))
}