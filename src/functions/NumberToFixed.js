export const numberRounded = (number) => { 
    if (number) {
        return Number(number).toFixed(2);
    }
    return '';
 }