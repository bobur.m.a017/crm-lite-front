import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { baseUrl } from '../../utils/Strings'
import { getToken } from '../../functions/Tokens';
import { axiosBaseQuery } from './AxiosBaseQuery';
import { baseUrlFn } from '../../functions/Url';

export const inputProductApi = createApi({
  reducerPath: 'inputProductApi',
  baseQuery: axiosBaseQuery({ baseUrl: baseUrlFn() }),
  tagTypes: ["addinputProduct"],
  endpoints: (builder) => ({
    getinputProducts: builder.query({
      query: (data) => (
        {
          url: `input-product`,
          headers: {
            Accept: "application/json",
            "Authorization": "Bearer " + getToken()
          },
          params: 
            data
          ,
        }),
      providesTags: ["addinputProduct"]
    }),
    addinputProduct: builder.mutation({
      query: (data) => ({
        url: `input-product`,
        method: 'POST',
        body: data,
        headers: {
          Accept: "application/json",
          "Authorization": "Bearer " + getToken()
        }
      }),
      invalidatesTags: ['addinputProduct']
    }),
    editeinputProduct: builder.mutation({
      query: (data) => ({
        url: `input-product/` + data?.id,
        method: 'PUT',
        body: data,
        headers: {
          Accept: "application/json",
          "Authorization": "Bearer " + getToken()
        }
      }),
      invalidatesTags: ['addinputProduct']
    }),
    deleteinputProduct: builder.mutation({
      query: (data) => ({
        url: `input-product/` + data?.id,
        method: 'DELETE',
        headers: {
          Accept: "application/json",
          "Authorization": "Bearer " + getToken()
        }
      }),
      invalidatesTags: ['addinputProduct']
    }),
    getinputProduct: builder.query({
      query: () => (
        {
          url: `input-product`,
          method: 'GET',
          headers: {
            Accept: "application/json",
            "Authorization": "Bearer " + getToken()
          },
        }
      ),
      invalidatesTags: ['addinputProduct']
    }),
  }),
})
export const {
  useAddinputProductMutation,
  useGetinputProductsQuery,
  useGetinputProductQuery,
  useEditeinputProductMutation,
  useDeleteinputProductMutation,
} = inputProductApi;