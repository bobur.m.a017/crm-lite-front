import { createApi } from '@reduxjs/toolkit/query/react'
import { getToken } from '../../functions/Tokens';
import { axiosBaseQuery } from './AxiosBaseQuery';
import { baseUrlFn } from '../../functions/Url';

export const statisticApi = createApi({
  reducerPath: 'statisticApi',
  baseQuery: axiosBaseQuery({ baseUrl: baseUrlFn() }),
  tagTypes: ["addstatistic"],
  endpoints: (builder) => ({
    getTotalStatistics: builder.query({
      query: (data) => (
        {
          url: `statistic/total`,
          headers: {
            Accept:"application/json",
            "Authorization": "Bearer " + getToken()
          },
          params: data,
        }),
      providesTags: ["addstatistic"]
    }),
    getTopProducts: builder.query({
      query: (data) => (
        {
          url: `statistic/top`,
          headers: {
            Accept:"application/json",
            "Authorization": "Bearer " + getToken()
          },
          params: data,
        }),
      providesTags: ["addstatistic"]
    }),
    getResiduals: builder.query({
      query: (data) => (
        {
          url: `statistic/residual`,
          headers: {
            Accept:"application/json",
            "Authorization": "Bearer " + getToken()
          },
          params: data,
        }),
      providesTags: ["addstatistic"]
    }),
    getWeek: builder.query({
      query: (data) => (
        {
          url: `statistic/week`,
          headers: {
            Accept:"application/json",
            "Authorization": "Bearer " + getToken()
          },
          params: data,
        }),
      providesTags: ["addstatistic"]
    }),
    
  }),
})

export const {
  useGetTotalStatisticsQuery,
  useGetTopProductsQuery,
  useGetResidualsQuery,
  useGetWeekQuery
} = statisticApi;