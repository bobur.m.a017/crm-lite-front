import { createApi } from '@reduxjs/toolkit/query/react'
import { getToken } from '../../functions/Tokens';
import { axiosBaseQuery } from './AxiosBaseQuery';
import { baseUrlFn } from '../../functions/Url';

export const productApi = createApi({
  reducerPath: 'productApi',
  baseQuery: axiosBaseQuery({ baseUrl: baseUrlFn() }),
  tagTypes: ["addProduct"],
  endpoints: (builder) => ({
    getProducts: builder.query({
      query: (data) => (
        {
          url: `product`,
          headers: {
            Accept:"application/json",
            "Authorization": "Bearer " + getToken()
          },
          params: data,
        }),
      providesTags: ["addProduct"]
    }),
    getProductList: builder.query({
      query: (data) => (
        {
          url: `product/list`,
          headers: {
            Accept:"application/json",
            "Authorization": "Bearer " + getToken()
          },
          params: data,
        }),
      providesTags: ["addProduct"]
    }),
    addProduct: builder.mutation({
      query: (data) => ({
        url: `product`,
        method: 'POST',
        body: data,
        headers: {
          Accept:"application/json",
          "Authorization": "Bearer " + getToken()
        }
      }),
      invalidatesTags: ['addProduct']
    }),
    editeProduct: builder.mutation({
      query: (data) => ({
        url: `product/` + data?.id,
        method: 'PUT',
        body: data,
        headers: {
          Accept:"application/json",
          "Authorization": "Bearer " + getToken()
        }
      }),
      invalidatesTags: ['addProduct']
    }),
    deleteProduct: builder.mutation({
      query: (data) => ({
        url: `product/` + data?.id,
        method: 'DELETE',
        headers: {
          Accept:"application/json",
          "Authorization": "Bearer " + getToken()
        }
      }),
      invalidatesTags: ['addProduct']
    }),
    getproduct: builder.query({
      query: () => (
        {
          url: `user`,
          method: 'GET',
          headers: {
            Accept:"application/json",
            "Authorization": "Bearer " + getToken()
          },
        }
      ),
      invalidatesTags: ['addProduct']
    }),
  }),
})
export const {
  useAddProductMutation,
  useLazyGetProductsQuery,
  useGetproductQuery,
  useEditeProductMutation,
  useLazyGetProductListQuery,
  useDeleteProductMutation
} = productApi;