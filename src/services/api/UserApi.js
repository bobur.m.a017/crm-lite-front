import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { baseUrl } from '../../utils/Strings'
import { getToken } from '../../functions/Tokens';
import { axiosBaseQuery } from './AxiosBaseQuery';
import { baseUrlFn } from '../../functions/Url';

export const userApi = createApi({
  reducerPath: 'userApi',
  baseQuery: axiosBaseQuery({ baseUrl: baseUrlFn() }),
  endpoints: (builder) => ({
    loginUser: builder.mutation({
      query: (data) => ({
        url: `login`,
        method: 'POST',
        headers: {
          Accept:"application/json",
          "Authorization": "Bearer " + getToken()
        },
        body: data,
      }),
    }),
    budgetPhone: builder.mutation({
      query: (data) => ({
        url: `userr`,
        method: 'POST',
        headers: {
          Accept:"application/json",
          "Authorization": "Bearer " + getToken()
        },
        body: {phoneNumber:data},
      }),
    }),
    getUser: builder.query({
      query: () => (
        {
          url: `user`,
          method: 'GET',
          headers: {
            Accept:"application/json",
            "Authorization": "Bearer " + getToken()
          },
        }),
    }),
    
  }),
})
export const {
  useGetUserQuery,
  useLoginUserMutation,
  useBudgetPhoneMutation,
} = userApi;