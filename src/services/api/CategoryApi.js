import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { baseUrl } from '../../utils/Strings'
import { getToken } from '../../functions/Tokens';
import { axiosBaseQuery } from './AxiosBaseQuery';
import { baseUrlFn } from '../../functions/Url';

export const categoryApi = createApi({
  reducerPath: 'categoryApi',
  baseQuery: axiosBaseQuery({ baseUrl: baseUrlFn() }),
  tagTypes: ["addCategory"],
  endpoints: (builder) => ({
    getCategories: builder.query({
      query: ({ shop_id }) => (
        {
          url: `category`,
          headers: {
            Accept:"application/json",
            "Authorization": "Bearer " + getToken()
          },
          params: {
            shop_id
          },
        }),
      providesTags: ["addCategory"]
    }),
    getCategoryList: builder.query({
      query: (data) => (
        console.log(data,"data"),
        {
          url: `category/list`,
          headers: {
            Accept:"application/json",
            "Authorization": "Bearer " + getToken()
          },
          params: data,
        }),
      providesTags: ["addCategory"]
    }),
    addCategory: builder.mutation({
      query: (data) => ({
        url: `category`,
        method: 'POST',
        body: data,
        headers: {
          Accept:"application/json",
          "Authorization": "Bearer " + getToken()
        }
      }),
      invalidatesTags: ['addCategory']
    }),
    editeCategory: builder.mutation({
      query: (data) => ({
        url: "category/"+data.id,
        method: 'PUT',
        body: data,
        headers: {
          Accept:"application/json",
          "Authorization": "Bearer " + getToken()
        }
      }),
      invalidatesTags: ['addCategory']
    }),
    getCategory: builder.query({
      query: () => (
        {
          url: `user`,
          method: 'GET',
          headers: {
            Accept:"application/json",
            "Authorization": "Bearer " + getToken()
          },
        }
      ),
      providesTags: ['addCategory']
    }),
    deleteCategory: builder.mutation({
      query: (data) => (
        {
          url: `category/${data.id}`,
          method: 'DELETE',
          headers: {
            Accept:"application/json",
            "Authorization": "Bearer " + getToken()
          },
        }
      ),
      invalidatesTags: ['addCategory']
    }),
  }),
})
export const {
  useAddCategoryMutation,
  useGetCategoriesQuery,
  useLazyGetCategoriesQuery,
  useGetCategoryQuery,
  useEditeCategoryMutation,
  useDeleteCategoryMutation,
  useGetCategoryListQuery,
  useLazyGetCategoryListQuery
} = categoryApi;