import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { getToken } from '../../functions/Tokens';
import { axiosBaseQuery } from './AxiosBaseQuery';
import { baseUrlFn } from '../../functions/Url';

export const outputProductApi = createApi({
  reducerPath: 'outputProductApi',
  baseQuery: axiosBaseQuery({ baseUrl: baseUrlFn() }),
  tagTypes: ["addOutputProduct"],
  endpoints: (builder) => ({
    getOutputProducts: builder.query({
      query: (data) => (
        {
          url: `output-product`,
          headers: {
            Accept: "application/json",
            "Authorization": "Bearer " + getToken()
          },
          params:data,
        }),
      providesTags: ["addOutputProduct"]
    }),
    addOutputProduct: builder.mutation({
      query: (data) => ({
        url: `output-product`,
        method: 'POST',
        body: data,
        headers: {
          Accept: "application/json",
          "Authorization": "Bearer " + getToken()
        }
      }),
      invalidatesTags: ['addOutputProduct']
    }),
    editeOutputProduct: builder.mutation({
      query: (data) => ({
        url: `output-product/` + data?.id,
        method: 'PUT',
        body: data,
        headers: {
          Accept: "application/json",
          "Authorization": "Bearer " + getToken()
        }
      }),
      invalidatesTags: ['addOutputProduct']
    }),
    deleteOutputProduct: builder.mutation({
      query: (data) => ({
        url: `output-product/` + data?.id,
        method: 'DELETE',
        headers: {
          Accept: "application/json",
          "Authorization": "Bearer " + getToken()
        }
      }),
      invalidatesTags: ['addOutputProduct']
    }),
    getOutputProduct: builder.query({
      query: () => (
        {
          url: `output-product`,
          method: 'GET',
          headers: {
            Accept: "application/json",
            "Authorization": "Bearer " + getToken()
          },
        }
      ),
      invalidatesTags: ['addOutputProduct']
    }),
  }),
})
export const {
  useAddOutputProductMutation,
  useGetOutputProductsQuery,
  useGetOutputProductQuery,
  useEditeOutputProductMutation,
  useDeleteOutputProductMutation,
} = outputProductApi;