import { createApi } from '@reduxjs/toolkit/query'
import axios from 'axios'
import { toast } from 'react-toastify';

export const axiosBaseQuery =
  ({ baseUrl } = { baseUrl: '' }) =>
    async ({ url, method, body, params, headers }) => {
      try {
        const result = await axios({
          url: baseUrl + url,
          method,
          data: body,
          params,
          headers,
        })
        toast.success(result?.data?.message);
        toast.success(result?.data);
        console.log(result.data)
        return { data: result.data }
      } catch (axiosError) {
        const err = axiosError
        console.log(err.response)
        toast.error(err.data?.message || err.message)
        return {
          error: {
            status: err.response?.status,
            data: err.response?.data || err.message,
          },
        }
      }
    }