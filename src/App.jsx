import React from 'react'
import ReactDOM from 'react-dom/client'
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import Main from './pages/Main';
import './index.css';
import { Provider } from 'react-redux'
import { store } from './store/ConfigureStore';
import 'bootstrap/dist/css/bootstrap.min.css';

const router = createBrowserRouter([
    {
      path: "/*",
      element: <Main/>,
      errorElement:<div>Bunaqa yo'l topilmadi</div>
    },
  ]);

const app = document.getElementById('root');
ReactDOM.createRoot(app).render(
  <Provider store={store}>
    <RouterProvider router={router} />
  </Provider>
);