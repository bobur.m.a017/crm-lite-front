import { configureStore } from '@reduxjs/toolkit'
// Or from '@reduxjs/toolkit/query/react'
import { setupListeners } from '@reduxjs/toolkit/query'
import { userApi } from '../services/api/UserApi.js'
import { categoryApi } from '../services/api/CategoryApi.js'
import reducerStore from './reducerSlices/UserSlice';
import { productApi } from '../services/api/ProductApi.js';
import { inputProductApi } from '../services/api/InputProductApi.js';
import { outputProductApi } from '../services/api/OutputProductApi.js';
import { statisticApi } from '../services/api/StatisticApi.js';

export const store = configureStore({
  reducer: {
    // Add the generated reducer as a specific top-level slice
    [statisticApi.reducerPath]: statisticApi.reducer,
    [inputProductApi.reducerPath]: inputProductApi.reducer,
    [outputProductApi.reducerPath]: outputProductApi.reducer,
    [userApi.reducerPath]: userApi.reducer,
    [productApi.reducerPath]: productApi.reducer,
    [categoryApi.reducerPath]: categoryApi.reducer,
    reducerStore
  },
  // Adding the api middleware enables caching, invalidation, polling,
  // and other useful features of `rtk-query`.
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([
      userApi.middleware,
      categoryApi.middleware,
      productApi.middleware,
      inputProductApi.middleware,
      outputProductApi.middleware,
      statisticApi.middleware,
    ]),
})

// optional, but required for refetchOnFocus/refetchOnReconnect behaviors
// see `setupListeners` docs - takes an optional callback as the 2nd arg for customization
setupListeners(store.dispatch)