import { createSlice } from "@reduxjs/toolkit";
const reducerToolkit = createSlice({
    name: 'reducerStore',
    initialState: {
        user: {},
        selected_shop: {id:1},
        loader: {},
        error: {},
        success: {},
    },
    reducers: {
        addUserReducer(state, action) {
            state.user = action.payload
        },
        selectShopReducer(state, action) {
            state.selected_shop = action.payload
        },
        activeLoader(state, action) {
            state.loader = action.payload
        },
    }
});

export default reducerToolkit.reducer;
export const { activeLoader, addUserReducer,selectShopReducer } = reducerToolkit.actions;