import React from 'react'
import { Form } from 'react-bootstrap';

function MonthForNav({setParams, params,name}) {
  return (
    <div className='ststisticNav'>
        <h5 className='text-center'>{name}</h5>
        <nav className='flex flex-wrap justify-around'>
                <div className='min-w-50 flex px-2 items-center mt-2'>
                    <span className='mr-2'>Oy</span>
                    <Form.Control
                        size='sm'
                        onChange={(e) => setParams({ ...params, month: e.target.value })}
                        type='month'
                        value={params?.month}
                        name='start_date'
                        placeholder="Sanadan"
                        aria-label="start date"
                        aria-describedby="basic-addon1"
                    />
                </div>
            </nav>
    </div>
  )
}

export default MonthForNav