import React, { useEffect, useState } from 'react';
import Outside from './Outside';
import { Button, Form } from 'react-bootstrap';
import "./searchStyle.scss";


const SelectWithSearchFromOutput = ({ options, selectData, labelInput, dataValue ,index}) => {
  const [inputValue, setinputValue] = useState(dataValue);
  const [optionsState, setoptionsState] = useState(options);
  const [activeSearch, setactiveSearch] = useState(false);

  useEffect(() => {
    if (dataValue?.id) {
      setinputValue({ ...inputValue, nameValue: dataValue?.name });
    }
    setoptionsState(options);
  }, [options])


  const changeInput = (e) => {
    const list = options?.filter((item) => item.name.toLowerCase().includes(e.target.value.toLowerCase()));
    if (list?.length === 1) {
      setinputValue({ ...list[0], nameValue: e.target.value });
    } else {
      setinputValue({ ...inputValue, nameValue: e.target.value });
    }
    setoptionsState(list);
  }
  const onClickProduct = (params) => {
    setinputValue({ ...params, nameValue: params.name });
    selectData(index, params);
    setactiveSearch(false);
  }
  return (
    <div>
      <Outside active={setactiveSearch}>
        <div className='search relative'>
        {/* <div className='search'> */}
          <Form.Label>{labelInput}</Form.Label>
          <Form.Control className='input' required autoComplete='off' name='serachInput' value={inputValue?.nameValue || ''} size='sm'
            onFocus={() => setactiveSearch(true)} onChange={changeInput} />
          {activeSearch ? <li className='my-li absolute p-2'>
            {
              optionsState?.map((item, index) =>
                <ul className='my-ul' key={index} onClick={() => onClickProduct(item)}>{item.name}</ul>
              )
            }
            {inputValue?.nameValue !== '' && optionsState?.length < 1 ? <ul className='text-xs text-rose-700'>Hech nima topilmadi</ul> : null}
          </li> : null}
        </div>
      </Outside>
    </div>
  )
};

export default SelectWithSearchFromOutput;
