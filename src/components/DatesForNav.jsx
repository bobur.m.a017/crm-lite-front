import React from 'react'
import { Form } from 'react-bootstrap';

function DatesForNav({ setParams, params,name }) {
    return (
        <div className='ststisticNav'>
            <h5 className='text-center'>{name}</h5>
            <nav className='flex flex-wrap justify-around'>
                <div className='min-w-50 flex px-2 items-center mt-2'>
                    <span className='mr-2'>Sanadan</span>
                    <Form.Control
                        size='sm'
                        onChange={(e) => setParams({ ...params, start_date: e.target.value })}
                        type='date'
                        value={params?.start_date||''}
                        name='start_date'
                        placeholder="Sanadan"
                        aria-label="start date"
                        aria-describedby="basic-addon1"
                    />
                </div>
                <div className='min-w-50 flex items-center mt-2'>
                    <span className='mr-2'>Sanagacha</span>
                    <Form.Control
                        size='sm'
                        value={params?.end_date||''}
                        onChange={(e) => setParams({ ...params, end_date: e.target.value })}
                        name='end_date'
                        type='date'
                        placeholder="Sanadan"
                        aria-label="start date"
                        aria-describedby="basic-addon1"
                    />
                </div>
            </nav>
        </div>
    )
}

export default DatesForNav