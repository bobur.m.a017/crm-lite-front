import React, { useState } from 'react'
import { Button, Dropdown, Form } from 'react-bootstrap';


function DropdownCustom({ list, selectedData,name }) {

    const [thisDataSelected, setThisDataSelected] = useState({name:name})
    const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
        <Button
            ref={ref}
            size='sm'
            variant='outline-success'
            onClick={(e) => {
                e.preventDefault();
                onClick(e);
            }}
        >
            {children}
        </Button>
    ));
    const [value, setValue] = useState('');
    const CustomMenu = React.forwardRef(
        ({ children, style, className, 'aria-labelledby': labeledBy }, ref) => {
    
            return (
                <div
                    ref={ref}
                    style={style}
                    className={className}
                    aria-labelledby={labeledBy}
                >
                    <Form.Control
                        autoFocus
                        className="mx-3 my-2 w-auto"
                        placeholder="Qidirish..."
                        onChange={(e) => setValue(e.target.value)}
                        value={value}
                    />
                    <ul className="list-unstyled">
                    <Dropdown.Item onClick={()=>selectEmty("new")} style={{color:'red'}}>Shu nom bilan yaratish</Dropdown.Item>
                    </ul>
                    <ul className="list-unstyled">
                        {React.Children.toArray(children).filter(
                            (child) =>
                                !value || child.props.children.toLowerCase().includes(value),
                        )}
                    </ul>
                </div>
            );
        },
    );
    const selectD = (params) => {
        selectedData(params);
        setThisDataSelected(params);
    }
    const selectEmty = (params) => {
        setThisDataSelected({name:value});
        selectedData(params);
    }
    
    return (
        <div>
            <Dropdown>
                <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
                    {thisDataSelected?.name || name}
                </Dropdown.Toggle>

                <Dropdown.Menu as={CustomMenu}>
                    {list?.map((element,index) => 
                        <Dropdown.Item eventKey={index} key={index} onClick={()=>selectD(element)}>{element?.name}</Dropdown.Item>
                    )}
                </Dropdown.Menu>
            </Dropdown>
        </div>
    )
}

export default DropdownCustom