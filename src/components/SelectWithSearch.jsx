import React, { useEffect, useState } from 'react';
import Outside from './Outside';
import { Button, Form } from 'react-bootstrap';
import "./searchStyle.scss";


const SelectWithSearch = ({ options, selectData ,labelInput ,dataValue}) => {
  const [inputValue, setinputValue] = useState(dataValue);
  const [optionsState, setoptionsState] = useState(options);
  const [activeSearch, setactiveSearch] = useState(false);

  useEffect(() => {
    if (dataValue?.id) {
      setinputValue({...inputValue,nameValue:dataValue?.name})
    }
    setoptionsState(options)
  }, [options])
  

  const changeInput = (e) => {
    const list = options?.filter((item) => item.name.toLowerCase().includes(e.target.value.toLowerCase()));
    if (list?.length === 1) {
      setinputValue({ ...list[0], nameValue: e.target.value });
    } else {
      setinputValue({ ...inputValue, nameValue: e.target.value });
    }
    setoptionsState(list);
  }
  const onClickProduct = (params) => {
    if (params === "new") {
      selectData(null, inputValue?.nameValue);
      setinputValue({ nameValue: inputValue.nameValue });
    } else {
      setinputValue({ ...params, nameValue: params.name });
      selectData("data", params);
    }
    setactiveSearch(false);
  }
  return (
    <div>
      <Outside active={setactiveSearch}>
        <div className='search relative w-[200px]'>
          <Form.Label>{labelInput}</Form.Label>
          <Form.Control className='input' required autoComplete='off' name='serachInput' value={inputValue?.nameValue||''} size='sm'
            onFocus={() => setactiveSearch(true)} onChange={changeInput} />
          {activeSearch ? <li className='ul absolute w-full'>
            {inputValue?.nameValue !== '' && optionsState?.length < 1 ? <ul className='ml-0 pl-0'> <Button variant='outline-info' size='sm' onClick={() => onClickProduct("new")} className='text-xs'>Shu nomda yangi qo'shish</Button></ul> : null}
            {
              optionsState?.map((item, index) =>
                <ul className='ul' key={index} onClick={() => onClickProduct(item)}>{item.name}</ul>
              )
            }
            {inputValue?.nameValue !== '' && optionsState?.length < 1 ? <ul className='text-xs text-rose-700'>Hech nima topilmadi</ul> : null}
          </li> : null}
        </div>
      </Outside>
    </div>
  )
};

export default SelectWithSearch;
