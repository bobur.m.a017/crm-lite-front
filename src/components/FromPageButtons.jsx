import React, { useState } from 'react'

function FromPageButtons({ current_page, last_page, click_page }) {
  const [current, setcurrent] = useState(1);
  const click_page2 = (second) => {
    setcurrent(second);
    click_page(second)
  }
  return (
    <div className='flex from-page-buttons'>
      {current_page !== 1  && current_page !== 2 && current_page !== 3 ? <span> <button onClick={() => click_page2(1)} style={current_page === 1 ? { backgroundColor: '#6763db', color: '#ffff' } : {}}>1</button> ... </span>:null}
      {current_page > 2 && <button onClick={() => click_page2(current_page-2)} style={current_page-2 === current ? { backgroundColor: '#6763db', color: '#ffff' } : {}}>{current_page - 2}</button>}
      {current_page > 1 && <button onClick={() => click_page2(current_page-1)} style={current_page-1 === current ? { backgroundColor: '#6763db', color: '#ffff' } : {}}>{current_page - 1}</button>}
      {<button onClick={() => click_page2(current_page)} style={current_page === current ? { backgroundColor: '#6763db', color: '#ffff' } : {}}>{current_page}</button>}
      {current_page < last_page && <button onClick={() => click_page2(current_page+1)} style={current_page+1 === current ? { backgroundColor: '#6763db', color: '#ffff' } : {}}>{current_page + 1}</button>}
      {current_page < last_page - 1 && <button onClick={() => click_page2(current_page+2)} style={current_page+2 === current ? { backgroundColor: '#6763db', color: '#ffff' } : {}}>{current_page + 2}</button>}
      {last_page - current_page > 2 ? <span> ... <button onClick={() => click_page2(last_page)} style={last_page === current ? { backgroundColor: '#6763db', color: '#ffff' } : {}}>{last_page}</button></span>:null}
    </div>
  )
}

export default FromPageButtons