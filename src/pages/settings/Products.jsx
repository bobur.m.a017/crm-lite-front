import React, { useEffect, useState } from 'react'
import { Modal, Button, Accordion, Card } from 'react-bootstrap';
import FromPageButtons from '../../components/FromPageButtons';
import { toLocalTime, toLocaldate } from '../../functions/DateFormats';
import { useAddProductMutation, useDeleteProductMutation, useEditeProductMutation, useLazyGetProductListQuery } from '../../services/api/ProductApi';
import { useSelector } from 'react-redux';
import { FaEdit } from 'react-icons/fa';
import { MdOutlineDelete } from 'react-icons/md';
import SelectWithSearch from '../../components/SelectWithSearch';
import { useGetCategoriesQuery, useLazyGetCategoriesQuery } from '../../services/api/CategoryApi';
import { Form } from 'react-bootstrap';

function Products() {
  const selectedShop = useSelector((state) => state.reducerStore?.selected_shop);
  const dto = { shop_id: selectedShop?.id, name: '', page: 1, category: '', category_id: '' };
  const [params, setparams] = useState(dto);
  const [getCategs,{ data: categories }] = useLazyGetCategoriesQuery();
  const [getProductList, {data} ] = useLazyGetProductListQuery();
  const [Product, setProduct] = useState(dto);
  const [addProduct] = useAddProductMutation();
  const [editeProduct] = useEditeProductMutation();
  const [deleteProduct] = useDeleteProductMutation();
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setProduct(dto);
    setShow(false)
  };
  const handleShow = () => setShow(true);
  useEffect(() => {
    getProductList(params);
    getCategs(params);
  }, [])

  const submitProduct = (e) => {
    e.preventDefault();
    if (Product?.id) {
      editeProduct(Product)
    } else {
      addProduct(Product);
      console.log(Product)
    }
    handleClose();
  }
  const editeProductFn = (data) => {
    let categ = categories?.filter(item => item?.id === data?.category_id)[0];
    setProduct({ ...data, category_id: categ?.id, category: categ });
    handleShow();
  }

  const getSelectCategory = (second, data) => {
    setProduct({ ...Product, category: second, category_id: data?.id });
  }
  const [timeoutId, setTimeoutId] = useState(null);

  const handleChange = (event) => {
    const value = event.target.value;
    // setInputValue(value);
    setparams({...params,name:event.target.value});


    // Clear the previous timeout if it exists
    if (timeoutId) {
      clearTimeout(timeoutId);
    }

    // Set a new timeout
    const newTimeoutId = setTimeout(() => {
      getProductList({ ...params, name: value });
    }, 1500);

    setTimeoutId(newTimeoutId);
  };

  return (
    <div>
      <Accordion>
        <Card>
          <Card.Header className='flex justify-between'>
            <Form>
              <Form.Control name='name' autoComplete='off'  value={params.name} onChange={handleChange} size='sm' placeholder='Mahsulot nomi' />
            </Form>
            <Button size='sm' className='m-2' onClick={handleShow} variant='outline-success'>Qo'shish</Button>
          </Card.Header>
        </Card>
      </Accordion>
      <div className='flex justify-center w-full'>
        <div className='md:w-[500px] overflow-x-auto'>
          <table className='myTable'>
            <thead>
              <tr>
                <th>#</th>
                <th>Nomi</th>
                <th>Vaqti</th>
                <th>Tahrirlash</th>
              </tr>
            </thead>
            <tbody>
              {
                data?.data?.map((item, index) =>
                  <tr key={index} className='myHover'>
                    <td>{index + 1}</td>
                    <td>{item?.name}</td>
                    <td>
                      {toLocaldate(item?.updated_at)}
                      <br />
                      {toLocalTime(item?.updated_at)}
                    </td>
                    <td>
                      <Button size='sm' variant='outline-primary' onClick={() => editeProductFn(item)}> <FaEdit /></Button>
                      <Button size='sm' variant='outline-danger' onClick={() => deleteProduct(item)}> <MdOutlineDelete /></Button>
                    </td>
                  </tr>
                )
              }
            </tbody>
          </table>
        </div>
      </div>
      <FromPageButtons current_page={data?.current_page} last_page={data?.last_page} click_page={setparams} />
      <Modal show={show} onHide={handleClose}>
        <Modal.Header>
          <Modal.Title>
            Mahsulot turi
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id='ProductForm' onSubmit={submitProduct}>
            <SelectWithSearch options={categories || []} selectData={getSelectCategory}
              labelInput={"Mahsulot turlari"} dataValue={Product?.category} />
            <Form.Label htmlFor="exampleFormControlInput1">Nomi</Form.Label>
            <Form.Control size='sm' type="text" className="max-w-[200px]" name='name' value={Product?.name || ''} id="exampleFormControlInput1" placeholder="Nomi" onChange={(e) => setProduct({ ...Product, name: e.target.value })} />

          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button type='submit' size='sm' variant='primary' form='ProductForm'>Tayyor</Button>
          <Button onClick={handleClose} size='sm' variant='danger'>Bekor qilish</Button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}

export default Products