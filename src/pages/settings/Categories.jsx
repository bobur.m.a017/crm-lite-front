import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { useAddCategoryMutation, useDeleteCategoryMutation, useEditeCategoryMutation, useGetCategoriesQuery, useGetCategoryListQuery, useLazyGetCategoryListQuery } from '../../services/api/CategoryApi';
import { Modal, Button, Accordion, Card, Form } from 'react-bootstrap';
import { MdOutlineDelete } from 'react-icons/md';
import { FaEdit } from 'react-icons/fa';
import { toLocalTime, toLocaldate } from '../../functions/DateFormats';
import FromPageButtons from './../../components/FromPageButtons';

function Categories() {
    const selectedShop = useSelector((state) => state.reducerStore?.selected_shop);
    const [params, setparams] = useState({ shop_id: selectedShop?.id, name: '', page: 1 });
    const [getCategories, { data }] = useLazyGetCategoryListQuery();
    const [category, setcategory] = useState({});
    const [addCategory] = useAddCategoryMutation();
    const [editeCategory] = useEditeCategoryMutation();
    const [deleteCategory] = useDeleteCategoryMutation();
    const [show, setShow] = useState(false);

    const handleClose = () => {
        setcategory({});
        setShow(false)
    };
    const handleShow = () => setShow(true);
    useEffect(() => {
        getCategories(params);
    }, [])

    const submitCategory = (e) => {
        e.preventDefault();
        if (category?.id) {
            editeCategory(category)
        } else {
            addCategory(category)
        }
        handleClose();
    }
    const editeCategoryFn = (data) => {
        setcategory(data);
        handleShow();
    }
  const [timeoutId, setTimeoutId] = useState(null);

    const handleChange = (event) => {
        const value = event.target.value;
        // setInputValue(value);
        setparams({...params,name:event.target.value});
    
    
        // Clear the previous timeout if it exists
        if (timeoutId) {
          clearTimeout(timeoutId);
        }
    
        // Set a new timeout
        const newTimeoutId = setTimeout(() => {
          getCategories({ ...params, name: value });
        }, 1500);
    
        setTimeoutId(newTimeoutId);
      };

    return (
        <div>
            <Accordion>
                <Card>
                    <Card.Header className='flex justify-between'>
                        <Form>
                            <Form.Control name='name' autoComplete='off' value={params?.name} onChange={handleChange} size='sm' placeholder='Mahsulot nomi' />
                        </Form>
                        <Button size='sm' className='m-2' onClick={handleShow} variant='outline-success'>Qo'shish</Button>
                    </Card.Header>
                </Card>
            </Accordion>
            <div className='flex justify-center w-full'>
                <div className='md:w-[500px] overflow-x-auto'>
                    <table className='myTable'>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nomi</th>
                                <th>Vaqti</th>
                                <th>Tahrirlash</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                data?.data?.map((item, index) =>
                                    <tr key={index} className='myHover'>
                                        <td>{index + 1}</td>
                                        <td>{item?.name}</td>
                                        <td>
                                            {toLocaldate(item?.updated_at)}
                                            <br />
                                            {toLocalTime(item?.updated_at)}
                                        </td>
                                        <td>
                                            <Button size='sm' variant='outline-primary' onClick={() => editeCategoryFn(item)}> <FaEdit /></Button>
                                            <Button size='sm' variant='outline-danger' onClick={() => deleteCategory(item)}> <MdOutlineDelete /></Button>
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
            <FromPageButtons current_page={data?.current_page} last_page={data?.last_page} click_page={setparams} />
            <Modal show={show} onHide={handleClose}>
                <Modal.Header>
                    <Modal.Title>
                        Mahsulot turi
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form id='categoryForm' onSubmit={submitCategory}>
                        <div className="form-group">
                            <label htmlFor="exampleFormControlInput1">Nomi</label>
                            <input type="text" className="form-control" name='name' value={category?.name || ''} id="exampleFormControlInput1" placeholder="Nomi" onChange={(e) => setcategory({ ...category, name: e.target.value })} />
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button type='submit' size='sm' variant='primary' form='categoryForm'>Tayyor</Button>
                    <Button onClick={handleClose} size='sm' variant='danger'>Bekor qilish</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export default Categories