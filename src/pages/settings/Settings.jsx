import React from 'react'
import Products from './Products'
import Categories from './Categories'

function Settings() {
  return (
    <div className='flex flex-wrap'>
      <Categories/>
      <Products/>
    </div>
  )
}

export default Settings