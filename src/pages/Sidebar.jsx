import React, { useEffect, useState } from 'react'
import { getToken } from '../functions/Tokens'
import './sidebar.scss'
import { useDispatch, useSelector } from 'react-redux';
import { NavLink, Route, Routes } from 'react-router-dom';
import { BsInfoSquare } from "react-icons/bs";
import { MdWarehouse } from "react-icons/md";
import { FaChartLine } from "react-icons/fa";
import { IoSettingsSharp } from "react-icons/io5";
import { IoExitSharp } from "react-icons/io5";
import { ImMenu } from "react-icons/im";
import User from './user/User';
import Statistics from './statistics/Statistics';
import Settings from './settings/Settings';
import Warehouse from './warehouse/Warehouse';
import SidebarPage from './sidebar/Sidebar';
import { selectShopReducer } from '../store/reducerSlices/UserSlice';
import { getShopeToLocalStorage, saveShopeToLocalStorage } from '../functions/Shop';

function Sidebar() {
    const user = useSelector(state => state.reducerStore.user);
    const shop = useSelector(state => state.reducerStore.selected_shop);
    const dispatch = useDispatch()
    if (!getToken()) {
        window.history.pushState(null, '', "/login");
        window.location.reload();
    }

    useEffect(() => {
        if (getShopeToLocalStorage()) {
            dispatch(selectShopReducer(getShopeToLocalStorage()));
        }
    }, [user?.shops_users]);

    const selectShop = (e) => {
        user?.shops_users?.forEach((item) => {
            if ((item.id == e.target.value)) {
                saveShopeToLocalStorage(item);
                dispatch(selectShopReducer(item));
            }
        })
    }

    return (
        <div>
            <div className="main">
                <div className="sidebar fixed">
                    <div className="desc">
                        <i><ImMenu size={25} /></i>
                        <span>Tizim</span>
                    </div>
                    <li>
                        <ul>
                            <NavLink className={({ isActive }) => isActive ? "isActive" : ""} to='/sidebar/user'>
                                <i><BsInfoSquare /></i>
                                <span className="">Bosh sahifa</span>
                            </NavLink>
                        </ul>
                        <ul>
                            <NavLink className={({ isActive }) => isActive ? "isActive" : ""} to='/sidebar/warehouse'>
                                <i><MdWarehouse size={25} /></i>
                                <span className="">Ombor</span>
                            </NavLink>
                        </ul>
                        <ul>
                            <NavLink className={({ isActive }) => isActive ? "isActive" : ""} to='/sidebar/statistics'>
                                <i><FaChartLine size={25} /></i>
                                <span className="">Statistika</span>
                            </NavLink>
                        </ul>
                        <ul>
                            <NavLink className={({ isActive }) => isActive ? "isActive" : ""} to='/sidebar/settings'>
                                <i ><IoSettingsSharp size={25} /></i>
                                <span className="">Sozlamalar</span>
                            </NavLink>
                        </ul>
                    </li>
                    <div className="out">
                        <NavLink className={({ isActive }) => isActive ? "isActive" : ""} to='/'>
                            <i><IoExitSharp size={25} /></i>
                            <span className="">Chiqish</span>
                        </NavLink>
                    </div>
                </div>
                <div className='flex w-full'>
                    <div className='sm:w-[45px] w-[0px]'>
                    </div>
                    <div className="content">
                        <nav className='sidebar-navbar'>
                            <div>
                                <div>{user?.user?.name}</div>
                            </div>
                            <div className='flex items-center'>
                                <div className='text-sm text-lime-800 font-bold'>Do'kon: </div>
                                <div>
                                    <select name="shopId" id="shopId" className='text-sm' value={shop?.id || getShopeToLocalStorage()?.id} onChange={selectShop}>
                                        {
                                            user?.shops_users?.map(element =>
                                                <option value={element.id} key={element.id} className='text-sm'>{element.name}</option>
                                            )
                                        }
                                    </select>
                                </div>
                            </div>
                        </nav>
                        <Routes>
                            {/* <Route path='/' element={<User />} /> */}
                            <Route path='/' element={<SidebarPage />} />
                            <Route path='/user' element={<User />} />
                            <Route path='/warehouse/*' element={<Warehouse />} />
                            <Route path='/settings' element={<Settings />} />
                            <Route path='/statistics' element={<Statistics />} />
                            <Route path='*' exact={true} element={<div>Bunaqa yul yuq</div>} />
                        </Routes>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Sidebar