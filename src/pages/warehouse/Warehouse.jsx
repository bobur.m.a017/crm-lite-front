import React from 'react'
import './style.scss';
import { IoSettingsSharp } from 'react-icons/io5'
import { NavLink, Route, Routes, useLocation } from 'react-router-dom'
import WarehouseBase from './WarehouseBase'
import InputProduct from './inputProduct/InputProduct'
import OutputProduct from './outputProduct/OutputProduct'
import { LuFileOutput } from "react-icons/lu";
import { LuFileInput } from "react-icons/lu";
import { PiWarehouseLight } from "react-icons/pi";

function Warehouse() {
  let location = useLocation();
  return (
    <div className='w-full'>
      <div className='navbar w-full'>
        <li className='w-full flex justify-center'>
          <ul>
            <NavLink className={location.pathname === "/sidebar/warehouse" ? "inActiveW" : "noActive"} to='/sidebar/warehouse'>
              <i ><PiWarehouseLight /></i>
              <span className="">Ombor</span>
            </NavLink>
          </ul>
          <ul>
            <NavLink className={({ isActive }) => isActive ? "inActiveW" : "noActive"} to='/sidebar/warehouse/input'>
              <i ><LuFileInput /></i>
              <span className="">Kirim</span>
            </NavLink>
          </ul>
          <ul>
            <NavLink className={({ isActive }) => isActive ? "inActiveW" : "noActive"} to='/sidebar/warehouse/output'>
              <i ><LuFileOutput /></i>
              <span className="">Chiqim</span>
            </NavLink>
          </ul>
        </li>
      </div>
      <Routes>
        <Route path='' element={<WarehouseBase/>} />
        <Route path='/input' element={<InputProduct/>} />
        <Route path='/output/*' element={<OutputProduct/>} />
        <Route path='*' exact={true} element={<div>Bunaqa yul yuq</div>} />
      </Routes>
    </div>
  )
}

export default Warehouse