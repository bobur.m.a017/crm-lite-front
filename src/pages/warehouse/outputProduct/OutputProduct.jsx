import React, { useState } from 'react'
import { Button } from 'react-bootstrap';
import OutputModal from './OutputModal';
import { useGetCategoriesQuery } from '../../../services/api/CategoryApi';
import { useSelector } from 'react-redux';
import { useGetOutputProductsQuery } from '../../../services/api/OutputProductApi';
import { NavLink,Route, Routes, useLocation  } from 'react-router-dom';
import './inputStyle.scss'
import OutputByProduct from './OutputByProduct';
import OutputByThread from './OutputByThread';
import DatesForNav from '../../../components/DatesForNav';

function OutputProduct() {
  const selectShope = useSelector(state => state.reducerStore?.selected_shop);
  const [params, setparams] = useState({ shop_id: selectShope?.id, page: 1 })
  const { data: outputProducts ,refetch} = useGetOutputProductsQuery(params);
  
  const { data } = useGetCategoriesQuery({ shop_id: selectShope?.id });
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const click_page = (second) => { 
    setparams({ ...params, page: second });
   }

  return (
    <div>
      <nav className='flex justify-between my-nav mt-2'>
        <li className='flex justify-start my-li'>
          <ul className=''>
            <NavLink className={({ isActive }) => isActive ? "inActiveW" : "noActive"} to='/sidebar/warehouse/output'>
              <Button variant='outline-success' size='sm'>
                <span className="">Mahsulot kesimida</span>
              </Button>
            </NavLink>
          </ul>
          <ul className=''>
            <NavLink className={({ isActive }) => isActive ? "inActiveW" : "noActive"} to='/sidebar/warehouse/output/purchase'>
              <Button variant='outline-success' size='sm'>
                <span className="">Xaridlar kesimida</span>
              </Button>
            </NavLink>
          </ul>
        </li>
        <div>

          <Button variant="success" onClick={handleShow} size='sm'>
            Sotish
          </Button>
        </div>
      </nav>
      <div>
        <DatesForNav setParams={setparams} params={params} name="Sana"/>
      </div>

      <Routes>
        <Route path='/' element={<OutputByProduct outputProducts={outputProducts} click_page={click_page}/>} />
        <Route path='/purchase' element={<OutputByThread outputProducts={outputProducts}  click_page={click_page}/>} />
      </Routes>
      <OutputModal show={show} handleClose={handleClose} categories={data} />
    </div>
  )
}

export default OutputProduct