import React, { useState } from 'react'
import { toLocalTime, toLocaldate } from '../../../functions/DateFormats'
import { Button, Form, Modal } from "react-bootstrap";
import { MdOutlineDelete } from "react-icons/md";
import { FaEdit } from "react-icons/fa";
import SelectWithSearch from "../../../components/SelectWithSearch";
import { useSelector } from "react-redux";
import { useGetCategoriesQuery } from "../../../services/api/CategoryApi";
import { useDeleteOutputProductMutation, useEditeOutputProductMutation } from '../../../services/api/OutputProductApi';
import FromPageButtons from '../../../components/FromPageButtons';

let dto = {
    id:"",
    category:"",
    product:"",
    count:"",
    price:"",
    product_id:"",
    price_debt:0,
    category_id:"",
    client_phone_number:"",
    client_name:"",
    debt:false,
}

function OutputByProduct({ outputProducts,click_page }) {
    const selectedShop = useSelector(state => state.reducerStore.selected_shop);
    const { data: categories } = useGetCategoriesQuery({ shop_id: selectedShop?.id });
    const [deleteOutputProduct] = useDeleteOutputProductMutation();
    const [show, setShow] = useState(false);
    const [outputProductDto, setOutputProductDto] = useState(dto);
    const [editeOutputProduct] = useEditeOutputProductMutation();


    const handleClose = () => {
        setShow(false);
        setOutputProductDto(dto);
    };
    const handleShow = () => setShow(true);



    function editeOutputProductModal(item) {
        categories?.forEach(category => {
            if (category?.id === item?.product?.category_id) {
                setOutputProductDto({
                    ...outputProductDto,
                    category: category,
                    category_id: category?.id,
                    product_id: item?.product_id,
                    id: item?.id,
                    product: item?.product,
                    count: item?.count,
                    price: item?.price,
                    price_debt: item?.price_debt,
                    debt: item?.debt
                });
            }
        })
        handleShow();
    }

    const getCategory = (data) => {
        setOutputProductDto({ ...outputProductDto, category: data, category_id: data?.id });
    }
    const getProduct = (data) => {
        setOutputProductDto({ ...outputProductDto, product: data, product_id: data?.id });
    };
    const changeInput = (e) => {
        setOutputProductDto({ ...outputProductDto, [e.target.name]: e.target.type === 'checkbox' ? e.target.checked : e.target.value });
    }

    const submitOutput = (e) => {
        e.preventDefault();
        editeOutputProduct(outputProductDto);
        handleClose();
    }
    return (
        <div>
            <div className='flex justify-center w-full'>
                <div className='md:w-[500px] scrollX'>
                    <table className='myTable'>
                        <thead>
                            <tr>
                                {/* <th>#</th> */}
                                <th>Nomi</th>
                                <th>Narxi</th>
                                <th>Miqdori</th>
                                <th>Umumiy narxi</th>
                                <th>Qarz</th>
                                <th>Vaqti</th>
                                <th>O'chirish</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                outputProducts?.data?.map((item, index) =>
                                    item?.output_products?.map((item2, index2) =>
                                        <tr key={index2} className='myHover'>
                                            <td>{item2?.product?.name}</td>
                                            <td>{item2?.price}</td>
                                            <td>{item2?.count}</td>
                                            <td>{item2?.count * item2?.price}</td>
                                            <td><span className={!item2?.debt ? 'text-green-500 font-bold' : 'text-red-500 font-bold'}>{item2?.price_debt}</span></td>
                                            <td>
                                                {toLocaldate(item2?.updated_at)}
                                                <br />
                                                {toLocalTime(item2?.updated_at)}
                                            </td>
                                            <td>
                                                <Button size='sm' variant='outline-primary'
                                                    onClick={() => editeOutputProductModal(item2)}> <FaEdit /></Button>
                                                <Button size='sm' variant='outline-danger'
                                                    onClick={() => deleteOutputProduct(item2)}>
                                                    <MdOutlineDelete /></Button>
                                            </td>
                                        </tr>
                                    )
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
                    <FromPageButtons current_page={outputProducts?.current_page} last_page={outputProducts?.last_page} click_page={click_page}/>
            
            <Modal show={show} fullscreen={""}>
                <Modal.Header >
                    <Modal.Title>Mahsulotni o'zgartirish</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={submitOutput} id='output-product'>
                        <div className={"flex flex-wrap justify-center"}>
                            <SelectWithSearch dataValue={outputProductDto?.category} options={categories}
                                labelInput={"Mahsulot turi"} selectData={getCategory} />
                            <SelectWithSearch dataValue={outputProductDto?.product}
                                options={outputProductDto?.category?.products} selectData={getProduct}
                                labelInput={"Mahsulot"} />
                            <Form.Group className="w-[200px]">
                                <Form.Label>Narxi</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Narxi"
                                    size={'sm'}
                                    className={""}
                                    autoFocus
                                    value={outputProductDto?.price}
                                    onChange={(e) => setOutputProductDto({ ...outputProductDto, price: e.target.value })}
                                />
                            </Form.Group>
                            <Form.Group className="w-[200px]" >
                                <Form.Label>Miqdori</Form.Label>
                                <Form.Control
                                    type="number"
                                    size={'sm'}
                                    className={""}
                                    placeholder="Miqdori"
                                    autoFocus
                                    value={outputProductDto?.count}
                                    onChange={(e) => setOutputProductDto({ ...outputProductDto, count: e.target.value })}
                                />
                            </Form.Group>
                            {outputProductDto.debt && <div className=''>
                                <Form.Label>Qarz narxi</Form.Label>
                                <Form.Control size='sm' type="number" placeholder="Qarz narxi" name='price_debt' required={outputProductDto.debt}
                                    value={outputProductDto.price_debt || ""} onChange={(e) => changeInput(e)} />
                            </div>}
                            {outputProductDto.debt && <div className=''>
                                <Form.Label>Tel nomer</Form.Label>
                                <Form.Control size='sm' type="text" placeholder="Tel nomer" required={outputProductDto.debt}
                                    name='client_phone_number' value={outputProductDto.client_phone_number || ""} onChange={(e) => changeInput(e)} />
                            </div>}
                            <div className='pl-[20px] pr-[20px] sm:pr-[50px] pt-[20px] '>
                                <Form.Check
                                    name='debt'
                                    onChange={(e) => changeInput(e)}
                                    checked={outputProductDto.debt || ''}
                                    className='text-[15px]'
                                    size='xxl'
                                    type="switch"
                                    id={"custom-switch"}
                                    label="Qarzga olish"
                                />
                            </div>
                        </div>

                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose} size={'sm'}>
                        Yopish
                    </Button>
                    <Button variant="primary" type='submit' form='output-product' size={'sm'}>
                        Tayyor
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export default OutputByProduct
