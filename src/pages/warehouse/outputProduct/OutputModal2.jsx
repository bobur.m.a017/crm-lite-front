import React, { useEffect, useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap';
import "../outputProduct/inputStyle.scss";
import SelectWithSearchFromOutput from '../../../components/SelectWithSearchFromOutput';
import { MdOutlineDelete } from "react-icons/md";

const dto = {
    shop_id: '',
    product_id: '',
    output_products: [
        {
            output_product_total_id: '',
            count: '',
            client_name: '',
            client_phone_number: '',
            username: '',
            price: '',
            price_debt: 0,
            product: {},
            category: {},
            debt: false,
        }
    ],

}
function OutputModal({ show, categories, handleClose }) {
    const [outputProductDto, setOutputProductDto] = useState(dto);
    const [outputList, setOutputList] = useState([]);

    useEffect(() => {

    }, [show])

    const selectCategory = (index, item) => {
        let outputList2 = [...outputList];
        outputList2[index].category = item;
        setOutputList(outputList2);
        console.log(outputList, "outputList");
    }

    const selectProduct = (index, item) => {
        let outputList2 = [...outputList];
        outputList2[index].product = item;
        setOutputList(outputList2);
        console.log(item, index);
    }
    const changeInput = (index, e) => {
        let outputList2 = [...outputList];
        outputList2[index][e.target.name] = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        setOutputList(outputList2);
        console.log(e.target?.checked, index);
        console.log(outputList2);
    }

    return (
        <div>
            <Modal show={show} fullscreen>
                <Modal.Header >
                    <Modal.Title ><span className='text-sm'>Mahsulotdan chiqim qilish</span></Modal.Title>
                </Modal.Header>
                <Modal.Body >
                    <Form className='overflow-x-auto'>
                        <div className='table'>
                            <table>
                                <thead>
                                    <tr>
                                        <th >No</th>
                                        <th >Mahsulot turlari</th>
                                        <th >Mahsulot</th>
                                        <th >Narxi</th>
                                        <th >Soni</th>
                                        <th >Umumiy summa</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        outputList?.map((item, index) =>
                                            <tr>
                                                <td className=''>{index+1}</td>
                                                <td className=''><SelectWithSearchFromOutput dataValue={item.category}
                                                    labelInput={"Mahsulot turlari"} options={categories} selectData={selectCategory} index={index} /></td>
                                                <td className=''><SelectWithSearchFromOutput dataValue={item.product}
                                                    labelInput={"Mahsulotlar"} options={item.category?.products} selectData={selectProduct} index={index} /></td>
                                                <td className=''><Form.Control size='sm' type="number" placeholder="Narxi" name='price' value={item.price} onChange={(e) => changeInput(index, e)} /></td>
                                                <td className=''><Form.Control size='sm' type="number" placeholder="Soni" name='count' value={item.count} onChange={(e) => changeInput(index, e)} /></td>
                                                <td className=''></td>
                                            </tr>
                                        )}
                                    {/* {
                                    outputList?.map((item, index) =>
                                        <div key={index} className='outputProduct mb-3 '>
                                            <div className='relative'>
                                                <div className='text-sm'>{index + 1}</div>
                                                <button type='button' className='absolute top-0 right-0'><MdOutlineDelete color='red' size={20} /></button>
                                            </div>
                                            <div className='flex justify-center flex-wrap max-w-full'>


                                                <div className='w-[200px]'>
                                                    <Form.Label>Narxi</Form.Label>

                                                </div>
                                                <div className='w-[200px]'>
                                                    <Form.Label>Soni</Form.Label>

                                                </div>
                                                <div>
                                                    <Form.Check
                                                        name='debt'
                                                        onChange={(e) => changeInput(index, e)}
                                                        checked={item.debt}
                                                        className='pr-[20px] sm:pr-[50px] pt-[20px] text-[15px]'
                                                        size='xxl'
                                                        type="switch"
                                                        id={"custom-switch" + index}
                                                        label="Qarzga olish"
                                                    />
                                                </div>
                                                {item.debt && <div className='w-[200px]'>
                                                    <Form.Label>Qarz narxi</Form.Label>
                                                    <Form.Control size='sm' type="number" placeholder="Qarz narxi" name='price' value={item.price} onChange={(e) => changeInput(index, e)} />
                                                </div>}
                                            </div>
                                        </div>
                                    )
                                } */}
                                </tbody>
                            </table>
                        </div>
                    </Form>
                    <Button onClick={() => setOutputList([...outputList, { ...dto.output_products[0] }])}>+</Button>
                </Modal.Body>
                <Modal.Footer>
                    <Button size='sm' variant="secondary" onClick={handleClose}>
                        Bekor qilish
                    </Button>
                    <Button size='sm' variant="primary" onClick={handleClose}>
                        Tayyor
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export default OutputModal