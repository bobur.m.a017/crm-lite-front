import React, { useEffect, useState } from 'react'
import { Accordion, Button, Form, Modal } from 'react-bootstrap';
import "../outputProduct/inputStyle.scss";
import SelectWithSearchFromOutput from '../../../components/SelectWithSearchFromOutput';
import { MdOutlineDelete } from "react-icons/md";
import { useAddOutputProductMutation } from '../../../services/api/OutputProductApi';
import { useSelector } from 'react-redux';

const dto = {
    shop_id: '',
    output_products: [
        {
            output_product_total_id: '',
            count: '',
            client_name: '',
            client_phone_number: '',
            username: '',
            price: '',
            price_debt: 0,
            product: {},
            category: {},
            debt: false,
        }
    ],

}
function OutputModal({ show, categories, handleClose }) {
    const selectShope = useSelector(state => state.reducerStore?.selected_shop);
    const [outputProductDto, setOutputProductDto] = useState(dto);
    const [addOutputProduct,{data:resultAdd}] =  useAddOutputProductMutation();
    const [outputList, setOutputList] = useState([]);
    const [render, setRender] = useState(true);
    const handleClose2 = () => {
        handleClose();
        setOutputProductDto(dto);
    }

    useEffect(() => {
        setRender(true);
    }, [outputList])

    const selectCategory = (index, item) => {
        let outputList2 = [...outputList];
        outputList2[index].category = item;
        outputList2[index].category_id = item?.id;
        setOutputList(outputList2);
    }

    const selectProduct = (index, item) => {
        let outputList2 = [...outputList];
        outputList2[index].product = item;
        outputList2[index].product_id = item?.id;
        setOutputList(outputList2);

    }
    const changeInput = (index, e) => {
        let outputList2 = [...outputList];
        outputList2[index][e.target.name] = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        setOutputList(outputList2);
    }
    const deleteOutput = (index) => {

        let outputList2 = [...outputList];
        outputList2.splice(index, 1);
        setRender(false);
        setOutputList(outputList2);

    }
    const submitOutput = (e) => { 
        e.preventDefault();
        addOutputProduct({...outputProductDto,shop_id:selectShope?.id,output_products:outputList});
        handleClose2(); //close moda
        
        console.log({...outputProductDto,shop_id:selectShope?.id,output_products:outputList}, "outputProductDto");
     }


    return (
        <div>
            <Modal show={show}>
                <Modal.Header >
                    <Modal.Title ><span className='text-sm'>Mahsulotdan chiqim qilish</span></Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={submitOutput} id='outputForm'>
                        <Accordion>
                            {
                                render === true ? outputList?.map((item, index) =>
                                    <div key={index} className='outputProduct mb-3 '>
                                        <Accordion.Item eventKey={index}>
                                            <Accordion.Header > <span className='text-[10px] sm:text-sm'><span className='font-bold'>{index + 1}. {item.product?.name}</span>  Umumiy narxi: <span className='font-bold'> {item.price * item.count} so'm </span> </span></Accordion.Header>
                                            <Accordion.Body>
                                                <div className='relative'>
                                                    <div className='text-sm'></div>
                                                    <button type='button' className='absolute top-[-10px] right-0 mb-10 z-1' onClick={() => deleteOutput(index)}><MdOutlineDelete color='red' size={30} /></button>
                                                </div>
                                                <div className='flex justify-center flex-wrap max-w-full'>
                                                    <SelectWithSearchFromOutput dataValue={item.category}
                                                        labelInput={"Mahsulot turlari"} options={categories} selectData={selectCategory} index={index} />
                                                    <SelectWithSearchFromOutput dataValue={item.product}
                                                        labelInput={"Mahsulotlar"} options={item.category?.products} selectData={selectProduct} index={index} />
                                                    <div className=''>
                                                        <Form.Label>Narxi</Form.Label>
                                                        <Form.Control size='sm' type="number" required placeholder="Narxi" name='price' value={item.price} onChange={(e) => changeInput(index, e)} />
                                                    </div>
                                                    <div className=''>
                                                        <Form.Label>Soni</Form.Label>
                                                        <Form.Control size='sm' type="number"  required placeholder="Soni" name='count' value={item.count} onChange={(e) => changeInput(index, e)} />
                                                    </div>
                                                    {item.debt && <div className=''>
                                                        <Form.Label>Qarz narxi</Form.Label>
                                                        <Form.Control size='sm' type="number" placeholder="Qarz narxi" name='price_debt' required={item.debt} value={item.price_debt} onChange={(e) => changeInput(index, e)} />
                                                    </div>}
                                                    {item.debt && <div className=''>
                                                        <Form.Label>Tel nomer</Form.Label>
                                                        <Form.Control size='sm' type="text" placeholder="Tel nomer" required={item.debt} name='client_phone_number' value={item.client_phone_number} onChange={(e) => changeInput(index, e)} />
                                                    </div>}
                                                    <div className='pl-[20px] pr-[20px] sm:pr-[50px] pt-[20px] '>
                                                        <Form.Check
                                                            name='debt'
                                                            onChange={(e) => changeInput(index, e)}
                                                            checked={item.debt}
                                                            className='text-[15px]'
                                                            size='xxl'
                                                            type="switch"
                                                            id={"custom-switch" + index}
                                                            label="Qarzga olish"
                                                        />
                                                    </div>
                                                </div>
                                            </Accordion.Body>
                                        </Accordion.Item>
                                    </div>
                                ) : null
                            }
                        </Accordion>
                    </Form>
                    <Button onClick={() => setOutputList([...outputList, { ...dto.output_products[0] }])}>+</Button>
                </Modal.Body>
                <Modal.Footer>
                    <Button size='sm' variant="secondary" onClick={handleClose2}>
                        Bekor qilish
                    </Button>
                    <Button size='sm' variant="primary" type='submit' form='outputForm'>
                        Tayyor
                    </Button>
                </Modal.Footer>
            </Modal>
        </div >
    )
}

export default OutputModal