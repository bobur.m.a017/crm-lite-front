import React, { useEffect, useState } from 'react'
import { toLocalTime, toLocaldate } from '../../../functions/DateFormats'
import FromPageButtons from '../../../components/FromPageButtons'

function OutputByThread({ outputProducts, click_page }) {
  const [first, setfirst] = useState([])
  useEffect(() => {
    setfirst(outputProducts);
  }, [outputProducts])

  return (
    <div>
      <div className='flex justify-center w-full'>
        <div className='md:w-[500px] overflow-x-auto'>
          <table className='myTable'>
            <thead>
              <tr>
                <th>#</th>
                <th>Xaridor ismi</th>
                <th> Xarid narxi</th>
                <th>Qarzi</th>
                <th>Vaqti</th>
              </tr>
            </thead>
            <tbody>
              {
                first?.data?.map((item, index) =>
                  <tr key={index} className='myHover'>
                    <td>{index + 1}</td>
                    <td>{item?.output_products?.length > 0 ? item?.output_products[0]?.client_phone_number : "Yo'q"}</td>
                    <td>{item?.output_products?.reduce((a, b) => { return a + parseInt(b.count) * parseFloat(b.price) }, 0)}</td>
                    <td className={ item?.output_products?.reduce((a, b) => { return parseFloat(b.price_debt) + a }, 0) > 0 ? 'text-red-500' : 'text-green-500'}>{item?.output_products?.reduce((a, b) => { return parseFloat(b.price_debt) + a }, 0)}</td>
                    <td>
                      {toLocaldate(item?.updated_at)}
                      <br />
                      {toLocalTime(item?.updated_at)}
                    </td>
                    <td>
                      {/* <Button size='sm' variant='outline-primary' onClick={()=>editeInputProduct(item)}> <FaEdit /></Button>
                      <Button size='sm' variant='outline-danger' onClick={()=>deleteInputProduct(item)}> <MdOutlineDelete /></Button> */}
                    </td>
                  </tr>
                )
              }
            </tbody>
          </table>
        </div>
      </div>
      <FromPageButtons current_page={outputProducts?.current_page} last_page={outputProducts?.last_page} click_page={click_page} />
    </div>
  )
}

export default OutputByThread