import React, { useEffect, useState } from 'react'
import { useLazyGetProductListQuery } from '../../services/api/ProductApi'
import { useSelector } from 'react-redux';
import { Accordion, Button, Card, Form, useAccordionButton } from 'react-bootstrap';
import { numberRounded } from '../../functions/NumberToFixed';
import FromPageButtons from './../../components/FromPageButtons';

function WarehouseBase() {
  function CustomToggle({ children, eventKey }) {
    const decoratedOnClick = useAccordionButton(eventKey, () =>
      console.log('totally custom!'),
    );

    return (
      <Button

        variant='light'
        type="button"
        size='sm'
        onClick={decoratedOnClick}
      >
        {children}
      </Button>
    );
  }
  const shop = useSelector(state => state.reducerStore.selected_shop);
  const [params, setparams] = useState({ shop_id: shop?.id, name: '' ,page:1});
  const [getProductApi, { data: products }] = useLazyGetProductListQuery();
  useEffect(() => {
    getProductApi(params);
  }, [])

  const changeInputSearch = (e) => {
    setparams({ ...params, name: e.target.value });
    getProductApi({ ...params, name: e.target.value });
  }

  const click_page = (page) => {
    setparams({ ...params, page: page });
    getProductApi({ ...params, page: page });
  }

  return (
    <div>
      <Accordion>
        <Card>
          <Card.Header className='flex justify-between'>
            <Form>
              <Form.Control name='name' value={params.name} onChange={changeInputSearch} size='sm' placeholder='Mahsulot nomi' />
            </Form>
            <Button size='sm' variant='success'>Qidirish</Button>
          </Card.Header>
        </Card>
      </Accordion>
      <div className='flex justify-center w-full'>
        <div className='md:w-[500px] '>
          <table className='myTable'>
            <thead>
              <tr>
                <th>#</th>
                <th>Nomi</th>
                <th>Narxi</th>
                <th>Miqdori</th>
                <th>Umumiy narxi</th>
              </tr>
            </thead>
            <tbody>
              {
                products?.data?.map((item, index) =>
                  <tr key={index} className='myHover'>
                    <td>{index + 1}</td>
                    <td>{item?.name}</td>
                    <td>{numberRounded(item?.price)}</td>
                    <td>{item?.count}</td>
                    <td>{numberRounded(item?.count * item?.price)}</td>
                  </tr>
                )
              }
            </tbody>
          </table>
        </div>
      </div>
        <FromPageButtons current_page={products?.current_page} last_page={products?.last_page} click_page={click_page}/>
    </div>
  )
}

export default WarehouseBase