import React, { useEffect, useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap';
import { useAddCategoryMutation, useGetCategoriesQuery } from '../../../services/api/CategoryApi';
import { useSelector } from 'react-redux';
import SelectWithSearch from '../../../components/SelectWithSearch';
import { useAddProductMutation } from '../../../services/api/ProductApi';
import { useAddinputProductMutation, useEditeinputProductMutation } from '../../../services/api/InputProductApi';

const dto = {
    id: '',
    category: '',
    product: '',
    price: '',
    count: '',
    total: 0,
}
function InputModal({ isOpen, close, inputProductState }) {
    const selectShope = useSelector(state => state.reducerStore.selected_shop);
    const [inputProductDto, setinputProductDto] = useState(dto);
    const { data, refetch } = useGetCategoriesQuery({ shop_id: selectShope?.id });
    const [addCategory, { data: resultCategory, error: categoryAddError }] = useAddCategoryMutation();
    const [addProduct, { data: resultproduct, error: productAddError }] = useAddProductMutation();
    const [addInputProduct, { data: resultInputProduct, error: inputProductAddError }] = useAddinputProductMutation();
    const [editeInputProduct, { data: resultInputProductEdite, error: inputProductEditeError }] = useEditeinputProductMutation();

    useEffect(() => {
        if (inputProductState?.id) {
            setinputProductDto({
                ...inputProductDto,
                id: inputProductState?.id,
                count: inputProductState?.count,
                price: inputProductState?.price,
                category: data?.filter(item => item.id === inputProductState?.product?.category_id)[0],
                product: inputProductState?.product,
            })
        }
        if (!isOpen) {
            setinputProductDto(dto);
        }
        setShow(isOpen);
    }, [isOpen])

    useEffect(() => {
        if (resultCategory?.id) {
            setinputProductDto({ ...inputProductDto, category: resultCategory });
        }
    }, [resultCategory]);


    useEffect(() => {
        data?.forEach(element => {
            if (element.id === inputProductDto?.category?.id) {
                setinputProductDto({ ...inputProductDto, category: element });
            }
        });

    }, [data])


    useEffect(() => {
        refetch();
        setinputProductDto({ ...inputProductDto, product: resultproduct })
    }, [resultproduct]);

    const [show, setShow] = useState(false);
    const handleClose = () => { close(false); setShow(false) };
    const handleShow = () => setShow(true);

    const getSelectCategory = (empty, data) => {
        if (empty) {
            setinputProductDto({ ...inputProductDto, category: data });
        } else {
            addCategory({
                name: data,
                shop_id: selectShope?.id
            });
        }
    }
    const getSelectProduct = (empty, data) => {
        if (empty) {
            setinputProductDto({ ...inputProductDto, product: data })
        } else {
            addProduct({
                name: data,
                shop_id: selectShope?.id,
                category_id: inputProductDto?.category?.id
            });
        }
    }

    const onChangeInputsProduct = (e) => {
        setinputProductDto({ ...inputProductDto, [e.target.name]: e.target.value });
    }
    const submitInputProduct = (e) => {
        e.preventDefault();
        if (inputProductDto.id) {
            editeInputProduct({
                id: inputProductDto.id,
                count: inputProductDto.count,
                price: inputProductDto.price,
                product_id: inputProductDto.product.id,
                shop_id: selectShope?.id,
            })
        } else {
            addInputProduct({
                count: inputProductDto.count,
                price: inputProductDto.price,
                product_id: inputProductDto.product.id,
                shop_id: selectShope?.id,
            });
        }
        console.log({
            id: inputProductDto.id,
            count: inputProductDto.count,
            price: inputProductDto.price,
            product_id: inputProductDto.product.id,
            shop_id: selectShope?.id,
        }, "inputProductDto");
        handleClose();
        setinputProductDto(dto);
    }
    return (
        <div>
            <Modal show={show} style={{ paddingLeft: 0 }}>
                <Modal.Header>
                    <Modal.Title>Mahsulot kirim qilish</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={submitInputProduct} id='inputProduct'>
                        <div className='sm:flex justify-around'>
                            <SelectWithSearch options={data || []} selectData={getSelectCategory}
                                labelInput={"Mahsulot turlari"} dataValue={inputProductDto.category} />


                            <SelectWithSearch options={inputProductDto?.category?.products || []}
                                selectData={getSelectProduct} labelInput={"Mahsulotlar"} dataValue={inputProductDto?.product} />
                        </div>
                        <div className='sm:flex justify-around'>
                            <div className='w-[200px]'>
                                <Form.Label>Narxi</Form.Label>
                                <Form.Control size='sm' required name='price' type='number' value={inputProductDto.price} step={"0.01"} onChange={onChangeInputsProduct} />
                            </div>
                            <div className='w-[200px]'>
                                <Form.Label>Miqdori</Form.Label>
                                <Form.Control size='sm' required name='count' type='number' value={inputProductDto.count} step={"0.01"} onChange={onChangeInputsProduct} />
                            </div>
                        </div>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" size='sm' onClick={handleClose}>
                        Yopish
                    </Button>
                    <Button variant="primary" type='submit' form={"inputProduct"} size='sm'>
                        Tayyor
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export default InputModal