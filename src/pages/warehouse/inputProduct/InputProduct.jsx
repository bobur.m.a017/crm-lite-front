import React, { useState } from 'react'
import { Accordion, Button, Card, Table, useAccordionButton } from 'react-bootstrap';
import { FaSearchDollar } from "react-icons/fa";
import InputModal from './InputModal';
import { useDeleteinputProductMutation, useGetinputProductsQuery } from '../../../services/api/InputProductApi';
import { useSelector } from 'react-redux';
import { toLocalTime, toLocaldate } from '../../../functions/DateFormats';
import { MdOutlineDelete } from "react-icons/md";
import { FaEdit } from "react-icons/fa";
import FromPageButtons from '../../../components/FromPageButtons';
import DatesForNav from '../../../components/DatesForNav';

function CustomToggle({ children, eventKey }) {
  const decoratedOnClick = useAccordionButton(eventKey, () =>
    console.log('totally custom!'),
  );

  return (
    <Button

      variant='light'
      type="button"
      size='sm'
      onClick={decoratedOnClick}
    >
      {children}
    </Button>
  );
}

function InputProduct() {
  const selectShope = useSelector(state => state.reducerStore.selected_shop);
  const [params, setparams] = useState({ shop_id: selectShope?.id, page: 1 })
  const { data,refetch } = useGetinputProductsQuery(params)
  const [inputProductState, setinputProductState] = useState()
  const [deleteInputProduct, { data: dleteData }] = useDeleteinputProductMutation();
  const [show, setshow] = useState(false);
  const closeModal = (params) => {
    setinputProductState({});
    setshow(params);
  }
  const editeInputProduct = (data) => {
    setinputProductState(data);
    setshow(true)
  }
  const click_page = (page) => {
   setparams({ ...params, page: page });
  //  refetch();
  }
  return (
    <div>
      <Accordion>
        <Card>
          <Card.Header className='flex justify-between'>
            <CustomToggle eventKey="0"><FaSearchDollar size={20} /></CustomToggle>
            <Button size='sm' variant='success' onClick={() => closeModal(true)}>Kirim</Button>
          </Card.Header>
          <Accordion.Collapse eventKey="0">
            <Card.Body>Hello! I'm the body</Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
      <DatesForNav setParams={setparams} params={params} name="Sana"/>
      <div className='flex justify-center w-full'>
        <div className='md:w-[500px] scrollX'>
          <table className='myTable'>
            <thead>
              <tr>
                <th>#</th>
                <th>Nomi</th>
                <th>Narxi</th>
                <th>Miqdori</th>
                <th>Umumiy narxi</th>
                <th>Vaqti</th>
                <th>O'chirish</th>
              </tr>
            </thead>
            <tbody>
              {
                data?.data?.map((item, index) =>
                  <tr key={index} className='myHover'>
                    <td>{index + 1}</td>
                    <td>{item?.product?.name}</td>
                    <td>{item?.price}</td>
                    <td>{item?.count}</td>
                    <td>{item?.count * item?.price}</td>
                    <td>
                      {toLocaldate(item.updated_at)}
                      <br />
                      {toLocalTime(item.updated_at)}
                    </td>
                    <td>
                      <Button size='sm' variant='outline-primary' onClick={() => editeInputProduct(item)}> <FaEdit /></Button>
                      <Button size='sm' variant='outline-danger' onClick={() => deleteInputProduct(item)}> <MdOutlineDelete /></Button>
                    </td>
                  </tr>
                )
              }
            </tbody>
          </table>
        </div>
      </div>
      <FromPageButtons current_page={data?.current_page} last_page={data?.last_page} click_page={click_page}/>
      <InputModal isOpen={show} close={closeModal} inputProductState={inputProductState} />
    </div>
  )
}

export default InputProduct