import React, { useEffect } from 'react'
import { Link, Route, Routes } from 'react-router-dom'
import User from './user/User'
import Login from './auth/Login'
import Sidebar from './Sidebar'
import { useDispatch } from 'react-redux'
import { useGetUserQuery } from '../services/api/UserApi'
import { addUserReducer } from '../store/reducerSlices/UserSlice'

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Main() {
  const { data } = useGetUserQuery()
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(addUserReducer(data));
  }, [data]);
  return (

    <div>
      <Routes>
        {/* <Route path='/' element={<div>Salom</div>} /> */}
        <Route path='/' element={<Login />} />
        <Route path='/user' element={<User />} />
        <Route path='/sidebar/*' element={<Sidebar />} />
        <Route path='*' exact={true} element={<div>Bunaqa yul yuq</div>} />
      </Routes>
      <ToastContainer />
    </div>

  )
}

export default Main