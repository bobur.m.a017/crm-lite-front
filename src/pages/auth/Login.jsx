import React, { useEffect } from 'react'
import './style.scss'
import './style.css'
import { useLoginUserMutation } from '../../services/api/UserApi'
import { getToken, saveToken } from '../../functions/Tokens';

function Login(props) {
    const [loginApi, data] = useLoginUserMutation();

    useEffect(() => {
        if (data.isSuccess) {
            saveToken(data.data?.access_token);
            window.history.pushState(null,'',"/sidebar");
            window.location.reload();
        }
    }, [data]);

    const formSubmit = (e) => {
        e.preventDefault();
        loginApi({
            username: e.target.username.value,
            password: e.target.password.value
        })
    }

    return (
        <div>
            <div className='body'>
                <div className="wrapper">
                    <div className="title">
                        Tizimga kirish
                    </div>
                    <form onSubmit={formSubmit}>
                        <div className="field">
                            <input type="text" required name='username' minLength={5} maxLength={18} />
                            <label>Login</label>
                        </div>
                        <div className="field">
                            <input type="password" required name='password' minLength={8} maxLength={18} />
                            <label>parol</label>
                        </div>

                        <div className="field">
                            <input type="submit" value="Kirish" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Login