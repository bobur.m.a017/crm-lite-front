import React, { useEffect, useState } from 'react'
import { useGetTopProductsQuery } from '../../../services/api/StatisticApi'
import { endDate, startDate } from '../../../functions/DateFormats';
import { useSelector } from 'react-redux';
import DatesForNav from '../../../components/DatesForNav';
import ReactApexChart  from 'react-apexcharts';

function TopProducts() {
    const selectedShop = useSelector((state) => state.reducerStore?.selected_shop);

    const [params, setParams] = useState({ shop_id: selectedShop?.id, start_date: startDate(), end_date: endDate() });
    const { data } = useGetTopProductsQuery(params);
    const chartDto = {
        series: [{
            name:"Sotilgan soni",
            data: data?.map((item) => item?.output_count_total)??[]
          }],
          options: {
            
            plotOptions: {
              bar: {
                borderRadius: 4,
                horizontal: true,
              }
            },
            dataLabels: {
              enabled: false
            },
            xaxis: {
              categories: data?.map((item) => item?.name)??["Malumot topilmadi"],
            }
          },
    }
    const [totalChart, settotalChart] = useState(chartDto);
    const [showChart, setshowChart] = useState(true);

    useEffect(() => {
        if (data?.input) {
            settotalChart(chartDto);

        }
    }, [data?.input])

    useEffect(() => {
        settotalChart(chartDto);
        setshowChart(false);
        setTimeout(() => {
            setshowChart(true);
        }, 1000)
    }, [params]);
    const heigthBar = () => { 
       return data?.length < 4 ? 100 : data?.length < 8 ? 200 : 500;
     }
    return (
        <div className='borderStatistics'>
            <DatesForNav setParams={setParams} params={params} name={"Ko'p sotilgan mahsulotlar"} />
            <div className='w-full'>
                {showChart ? <ReactApexChart options={chartDto.options} series={chartDto.series} width={'100%'} type='bar' height={heigthBar()} /> : ''}
            </div>
        </div>
    )
}

export default TopProducts