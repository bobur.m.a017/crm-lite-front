import React from 'react'
import DatesForNav from './../../../components/DatesForNav';
import { useGetResidualsQuery } from '../../../services/api/StatisticApi';
import { useSelector } from 'react-redux';

function Residual() {
  const selectedShop = useSelector((state) => state.reducerStore?.selected_shop);
  const { data } = useGetResidualsQuery({ shop_id: selectedShop?.id });
  return (
    <div className='borderStatistics w-full'>
      <div className='ststisticNav text-center'>
        <h5>Umumiy holat</h5>
      </div>
      <div className='flex flex-wrap'>
        <div className='card-statistics'>
          <span className='text-statistics'>Bor mahsulot turlari soni</span>
          <br />
          <span className='text-numbers'>{data?.products_category_count}</span>
        </div>
        <div className='card-statistics'>
          <span className='text-statistics'>Bor mahsulotlar soni</span>
          <br />
          <span className='text-numbers'>{data?.products_count}</span>
        </div>

        <div className='card-statistics'>
          <span className='text-statistics'>Umumiy narxi</span>
          <br />
          <span className='text-numbers'>{data?.product_prices_sum2}</span>
        </div>
        <div className='card-statistics'>
          <span className='text-statistics'>Umumiy qarzlar soni</span>
          <br />
          <span className='text-numbers'>{data?.debt_count}</span>
        </div>
        <div className='card-statistics'>
          <span className='text-statistics'>Umumiy qarz</span>
          <br />
          <span className='text-numbers'>{data?.debt_sum}</span>
        </div>
      </div>
    </div>
  )
}

export default Residual