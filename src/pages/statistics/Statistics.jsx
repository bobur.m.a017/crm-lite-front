import React from 'react'
import TotalStatistic from './totalStatistics/TotalStatistic'
import TopProducts from './topProduct/TopProducts'
import Residual from './residual/Residual'
import Weekly from './week/Weekly'
import './statisticStyle.scss'

function Statistics() {
  return (
    <div className='flex flex-wrap'>
      <TotalStatistic/>
      <TopProducts/>
      <Weekly/>
      <Residual/>
    </div>
  )
}

export default Statistics