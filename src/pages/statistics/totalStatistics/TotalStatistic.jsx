import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { useGetTotalStatisticsQuery } from '../../../services/api/StatisticApi';
import { endDate, startDate } from '../../../functions/DateFormats';
import DatesForNav from '../../../components/DatesForNav';
import ReactApexChart from 'react-apexcharts';
function TotalStatistic() {
    const selectedShop = useSelector((state) => state.reducerStore?.selected_shop);
    const [params, setParams] = useState({ shop_id: selectedShop?.id, start_date: startDate(), end_date: endDate() });
    const { data } = useGetTotalStatisticsQuery(params);
    const chartDto = {
        series: [parseFloat(data?.input), parseFloat(data?.output), parseFloat(data?.debt_price_total)],
        options:
        {
            labels: ["Kirim: " + data?.input, "Chiqim: " + data?.output, "Qarz: " + data?.debt_price_total],
            plotOptions: {
                pie: {
                    donut: {
                        size: '65%',
                        labels: {
                            show: true,
                            total: {
                                showAlways: true,
                                show: true,
                                label: "Foyda",
                                fontSize: '14px',
                                fontWeight: '600',
                                formatter: function (w) {
                                    return  data?.output - data?.input-data?.debt_price_total;
                                }
                            },
                            value: {
                                show: true,
                                fontSize: '14px',
                            }
                        }
                    }
                }
            },

        }

    }
    const [totalChart, settotalChart] = useState(chartDto);
    const [showChart, setshowChart] = useState(true);

    useEffect(() => {
        if (data?.input) {
            settotalChart(chartDto);

        }
    }, [data?.input])

    useEffect(() => {
        settotalChart(chartDto);
        setshowChart(false);
        setTimeout(() => {
            setshowChart(true);
        }, 1000)
    }, [params])

    return (
        <div className='borderStatistics'>
            <DatesForNav setParams={setParams} params={params} name={"Umumiy statistika"}/>
            <div className='w-[300px] sm:w-[400px]'>
                {showChart ? <ReactApexChart options={chartDto.options} series={chartDto.series} width={'100%'} type='donut' /> : ''}
            </div>
        </div>
    )
}

export default TotalStatistic