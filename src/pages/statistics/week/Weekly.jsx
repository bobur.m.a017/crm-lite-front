import React, { useEffect, useState } from 'react'
import MonthForNav from '../../../components/MonthForNav'
import { useSelector } from 'react-redux';
import { useGetWeekQuery } from '../../../services/api/StatisticApi';
import ReactApexChart  from 'react-apexcharts';

function Weekly() {
    const today = new Date();
    const year = today.getFullYear();
    const month = String(today.getMonth() + 1).padStart(2, '0'); // Zero-pad the month

    const currentMonth = `${year}-${month}`;
    const selectedShop = useSelector((state) => state.reducerStore?.selected_shop);
    const [params, setparams] = useState({ shop_id: selectedShop?.id, month: currentMonth });
    const { data } = useGetWeekQuery(params);
    const chartDto = {
        series: [{
            name: 'Kirim',
            data: data?.map((item) => item?.input)??[0,0,0,0]
          }, {
            name: 'Tushum',
            data: data?.map((item) => item?.output)??[0,0,0,0]
          }, {
            name: 'Qarz',
            data: data?.map((item) => item?.debt_price_total)??[0,0,0,0]
          }],
          options: {
            chart: {
              type: 'bar',
              // height: 350
            },
            plotOptions: {
              bar: {
                horizontal: false,
                columnWidth: '80%',
                endingShape: 'rounded'
              },
            },
            dataLabels: {
              enabled: false
            },
            stroke: {
              show: true,
              width: 3,
              colors: ['transparent']
            },
            xaxis: {
              categories: data?.map((item,index) => (index+1)+" - hafta")??["Malumot topilmadi"],
            },
            yaxis: {
              title: {
                text: "Umumiy narx (so'm)"
              }
            },
            fill: {
              opacity: 1
            },
            tooltip: {
              y: {
                formatter: function (val) {
                  return "$ " + val + " so'm"
                }
              }
            }
          },
        
        
    }
    const [totalChart, settotalChart] = useState(chartDto);
    const [showChart, setshowChart] = useState(true);

    useEffect(() => {
        if (data?.input) {
            settotalChart(chartDto);

        }
    }, [data?.input])

    useEffect(() => {
        settotalChart(chartDto);
        setshowChart(false);
        setTimeout(() => {
            setshowChart(true);
        }, 1000)
    }, [params]);
    const heigthBar = () => { 
       return data?.length < 4 ? 100 : data?.length < 8 ? 200 : 500;
     }
    return (
        <div className='borderStatistics w-full'>
            <MonthForNav setParams={setparams} params={params} name={"Haftalik statistika"}/>
            <div className='w-full'>
                {showChart ? <ReactApexChart options={chartDto.options} series={chartDto.series} width={'100%'} type='bar'  height={400}/> : ''}
            </div>
        </div>
    )
}

export default Weekly